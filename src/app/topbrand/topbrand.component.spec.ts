import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TopbrandComponent } from './topbrand.component';

describe('TopbrandComponent', () => {
  let component: TopbrandComponent;
  let fixture: ComponentFixture<TopbrandComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TopbrandComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TopbrandComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
