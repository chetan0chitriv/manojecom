import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GridslideComponent } from './gridslide.component';

describe('GridslideComponent', () => {
  let component: GridslideComponent;
  let fixture: ComponentFixture<GridslideComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GridslideComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GridslideComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
