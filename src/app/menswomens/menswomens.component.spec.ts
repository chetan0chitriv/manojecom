import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MenswomensComponent } from './menswomens.component';

describe('MenswomensComponent', () => {
  let component: MenswomensComponent;
  let fixture: ComponentFixture<MenswomensComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MenswomensComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MenswomensComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
