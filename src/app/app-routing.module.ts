import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BrandComponent } from './brand/brand.component';
import { ContactComponent } from './contact/contact.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { PrivacyComponent } from './privacy/privacy.component';
import { ProductComponent } from './product/product.component';
import { RegisterComponent } from './register/register.component';
import { ReturnComponent } from './return/return.component';
import { ShippingComponent } from './shipping/shipping.component';
import { TermsComponent } from './terms/terms.component';
import { TrackComponent } from './track/track.component';
import { UserComponent } from './user/user.component';

const routes: Routes = 
[
  { path: '', component :HomeComponent},
  { path: 'home', component:HomeComponent},
  { path: 'contact', component: ContactComponent },
  { path: 'return', component : ReturnComponent},
  { path: 'shipping', component:ShippingComponent },
  { path: 'terms', component:TermsComponent },
  { path: 'privacy', component:PrivacyComponent },
  { path: 'track', component:TrackComponent },
  { path: 'user', component :UserComponent},
  { path: 'login', component : LoginComponent},
  { path: 'register', component :RegisterComponent},
  { path: 'product', component : ProductComponent},
  { path: 'brand', component : BrandComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
