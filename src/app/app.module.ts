import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';


import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { ReturnComponent } from './return/return.component';
import { ShippingComponent } from './shipping/shipping.component';
import { TermsComponent } from './terms/terms.component';
import { PrivacyComponent } from './privacy/privacy.component';
import { TrackComponent } from './track/track.component';
import { HomeComponent } from './home/home.component';
import { ProductComponent } from './product/product.component';
import { BrandComponent } from './brand/brand.component';
import { ContactComponent } from './contact/contact.component';
import { UserComponent } from './user/user.component';
import { SliderComponent } from './slider/slider.component';
import { SubscribeComponent } from './subscribe/subscribe.component';
import { SummerComponent } from './summer/summer.component';
import { CallactionComponent } from './callaction/callaction.component';
import { OurfeatureComponent } from './ourfeature/ourfeature.component';
import { MenswomensComponent } from './menswomens/menswomens.component';
import { TopsellingComponent } from './topselling/topselling.component';
import { TopcatComponent } from './topcat/topcat.component';
import { TopbrandComponent } from './topbrand/topbrand.component';
import { CategoriesComponent } from './categories/categories.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { ResetComponent } from './reset/reset.component';
import { AboutusComponent } from './aboutus/aboutus.component';
import { FaqComponent } from './faq/faq.component';
import { SectionComponent } from './section/section.component';
import { GridslideComponent } from './gridslide/gridslide.component';




import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatListModule } from '@angular/material/list';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatCardModule } from '@angular/material/card';
import { MatSliderModule } from '@angular/material/slider';
import { FlexLayoutModule } from '@angular/flex-layout';








@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    ReturnComponent,
    ShippingComponent,
    TermsComponent,
    PrivacyComponent,
    TrackComponent,
    HomeComponent,
    ProductComponent,
    BrandComponent,
    ContactComponent,
    UserComponent,
    SliderComponent,
    SubscribeComponent,
    SummerComponent,
    CallactionComponent,
    OurfeatureComponent,
    MenswomensComponent,
    TopsellingComponent,
    TopcatComponent,
    TopbrandComponent,
    CategoriesComponent,
    LoginComponent,
    RegisterComponent,
    ResetComponent,
    AboutusComponent,
    FaqComponent,
    SectionComponent,
    GridslideComponent

    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    NgbModule,

    MatToolbarModule,
    MatIconModule,
    MatSidenavModule,
    MatListModule,
    MatButtonModule,
    MatInputModule,
    MatGridListModule,
    MatCardModule,
    MatSliderModule,

    FlexLayoutModule,


  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
